/*1. Input a string from user on command line. String may have multiple commas 
e.g. "Welcome,to,Sunbeam,CDAC,Diploma,Course". Print each word individually. 
Hint: use strtok() function.
*/



#include<stdlib.h>
#include<stdio.h>
#include<string.h>

int main()
{
	char str[]="Welcome,to,Sunbeam,CDAC,Diploma,Course";
	
	char *token;
	
	token=strtok(str,",");
	
	while(token!=NULL)
	{
		printf("%s\n",token);
		token=strtok(NULL,",");
	 } 
return 0;
}