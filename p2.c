
/*2 2. Simulate string library functions strcpy, strcat, strcmp and strrev.*/

#include <stdio.h>
#include <string.h>

int main(void)
{setvbuf(stdout,NULL,_IONBF,0);
   char str1[]="This code is for function strcpy()";
	char str2[50];
	strcpy(str2,str1);
	printf("Strcpy():\nStr1 = %s\nStr2 = %s",str1,str2);


	char str3[50], str4[50];
	strcpy(str3,  "This is source");
	printf("\n\nStrcat():\nStr1 = %s",str3);
	strcpy(str4, " This is destination");
	printf("\nStr2 = %s",str4);
	strcat(str3, str4);
	printf("\nFinal string : %s", str3);


	char str5[] = "stringcompare";
	printf("\n\nStrcmp():\nStr1 = %s",str5);
	char str6[] = "stringcompare";
	printf("\nStr2 = %s",str6);
	int res = strcmp(str5,str6);
	if (res==0)
		printf("\nStrings are equal");
	else
	    printf("\nStrings are unequal");
	printf("\nValue returned by strcmp() is: %d" , res);


	char str7[] = "sunbeam";
	printf("\n\nStrrev():\n");
	printf("The given string is = %s\n",str7);
	printf("After reversing string is = %s",strrev(str7));


	return 0;
}
