
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

// Function Declarations
void addNodeAtEnd();
void addNodeAtFront();
void displayListFromFront();
struct node* inputAccountDetails();
void displayNode(struct node* temp_node);
void deleteNodeFromEnd();
void deleteNodeFromFront();

// Structure of the account info which is the token
struct account_info {
	int account_id;
	char account_holder_name[20];
	char account_type[20];
	float account_balance;
	char address[40];
	int phone_number;
};

// Structure of the node of the linked list
struct node {
	struct node* prev;
	struct account_info* account;
	struct node* next;
};

// Linked-list references
struct node* head = NULL;
struct node* tail = NULL;

// Main: Starting point of the function
int main() {
	
	int choice;

	// Infinite loop for operations on linked list
	while(1) {
	
		printf("\n-------------------------MENU------------------------");
		printf("\n1. Add a node in the linked list at the end.");
		printf("\n2. Add a node in the linked list at the front.");
		printf("\n3. Display the linked list.");
		printf("\n4. Delete from the end of the linked list.");
		printf("\n5. Delete from the front of the linked list.");
		printf("\n0. Exit the program execution.");
		printf("\n-----------------------------------------------------");

		printf("\n\nEnter you choice: ");
		scanf("%d", &choice);

		switch(choice) {
	
			case 1:
				printf("Adding a node to end of the linked list.");
				addNodeAtEnd();
				break;

			case 2:
				printf("Adding a node at the front of the linked list.");
				addNodeAtFront();
				break;

			case 3:
				printf("Displaying the entire linked list.");
				displayListFromFront();
				break;

			case 4:
				printf("Deleting the elements from the end of the list.");
				deleteNodeFromEnd();
				break;

			case 5:
				printf("Deleting the elements from the start of the list.");
				deleteNodeFromFront();
				break;

			case 0:
				printf("Exiting the program.");
				exit(0);
				break;

			default: 
				printf("Please enter an appropriate choice.");
		}
		
	}
}

// Function containing logic to add a node at the end of the linked list
void addNodeAtEnd() {

	struct node* temp_node;
	temp_node = inputAccountDetails();
	if(head == NULL) {
		
		// The linked list is empty
		head = temp_node;
		tail = temp_node;
	} else {

		// Adding a node at the next location of the tail node
		tail->next = temp_node;
		temp_node->prev = tail;
		tail = temp_node;
	}
}

// Function containing logic to add a note at the front of the linked list
void addNodeAtFront() {

	struct node* temp_node;
	temp_node = inputAccountDetails();
	if (head == NULL) {

		// The linked list is empty
		head = temp_node;
		tail = temp_node;
	} else {

		// Adding a node at the previous location of head node
		head->prev = temp_node;
		temp_node->next = head;
		head = temp_node;
	}
}

// Function to input the account info in a single node
struct node* inputAccountDetails() {
	
	int account_id, phone_number;
	char account_holder_name[20], account_type[20], address[40];
	float account_balance;

	struct account_info* temp_account = (struct account_info*)malloc(sizeof(struct account_info));
	struct node* temp_node = (struct node*)malloc(sizeof(struct node));

	printf("\nEnter the details of the account information:\n");
	printf("\nI.D.:");
	scanf("%d", &account_id);
	temp_account->account_id = account_id;
	
	printf("Enter the name of the account holder:");
	scanf("%s", account_holder_name);
	strcpy(temp_account->account_holder_name, account_holder_name);

	printf("Enter the account type:");
	scanf("%s", account_type);
	strcpy(temp_account->account_type, account_type);

	printf("Enter the account balance:");
	scanf("%f", &account_balance);
	temp_account->account_balance = account_balance;

	printf("Enter the address:");
	scanf("%s", address);
	strcpy(temp_account->address, address);

	printf("Enter the phone number:");
	scanf("%d", &phone_number);
	temp_account->phone_number = phone_number;

	temp_node->prev = NULL;
	temp_node->account = temp_account;
	temp_node->next = NULL;

	// Returning the dynamically created node 
	return temp_node;
}

// Function containing logic that displays the linked list from the front
void displayListFromFront() {
	
	struct node* temp_node = head;
	if (head == NULL) {
	
		printf("The list is empty");
		return;
	}

	// Iterating over nodes until the next section of the node is NULL
	while(temp_node != NULL) {

		printf("\nThe account details:\n");	
		displayNode(temp_node);
		temp_node = temp_node->next;
	}

}

// Displaying an individual node
void displayNode(struct node* temp_node) {

	struct account_info* temp_account = temp_node->account;

	printf("\nI.D.: %d.", temp_account->account_id);
	printf("\nAccount Holder Name: %s.", temp_account->account_holder_name);
	printf("\nAccount Type: %s.", temp_account->account_type);
	printf("\nAccount Balance: %f.", temp_account->account_balance);
	printf("\nAddress: %s.", temp_account->address);
	printf("\nPhone number: %d.", temp_account->phone_number);
}

// Function containing logic that deletes node from the end of the list
void deleteNodeFromEnd() {

	if (head == NULL) {
		printf("The linked list is empty, and thus cannot delete anything.");
	} else {
		
		printf("The node being deleted is: ");
		displayNode(tail);
		if (tail->prev == NULL) {
			free(tail);
			head = tail = NULL;
		} else {
			tail = tail->prev;
			free(tail->next);
			tail->next = NULL;
		}
	}
}

// Function containing logic that deletes node from the front of the list
void deleteNodeFromFront() {

	if (head == NULL) {
		printf("The list is empty and hence cannot delete anything.");	
	} else {
	
		printf("The node being deleted is: ");
		displayNode(head);
		if (head->next == NULL) {
			free(head);
			head = tail = NULL;
		} else {
			head = head->next;
			free(head->prev);
			head->prev = NULL;
		}
	}
}
